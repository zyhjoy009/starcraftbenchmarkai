#ifndef LAUNCHER_H
#define LAUNCHER_H

#include <QtWidgets/QMainWindow>
#include <QTimer>
#include "ui_launcher.h"

class Launcher : public QMainWindow, private Ui::LauncherClass
{
	Q_OBJECT

public:
	Launcher(QWidget *parent = 0);
	~Launcher();

public slots:
	void runBenchmark();
	void checkGamesCompleted();

private:
	std::vector<QCheckBox*> mapsList;
	std::vector<std::string> mapsSelected;
	int currentMapIndex;
	QTimer* iTimerGames;

	_bstr_t		currentPath;
	_bstr_t		chaosLauncherPath;
	_bstr_t		logFileName;

	void printError(const QString & text);
	bool launchStarcraft();
	void stopStarcraft();
	void getStats();
};

#endif // LAUNCHER_H
